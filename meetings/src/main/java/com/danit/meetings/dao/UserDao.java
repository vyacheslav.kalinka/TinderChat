package com.danit.meetings.dao;


import com.danit.meetings.entity.User;

import java.util.ArrayList;
import java.util.List;


public class    UserDao implements UsersDao {
    private List<User> listUser = new ArrayList<>();

    public UserDao() {

        listUser.add(new User(1, "Anna", "http://www.gogetnews.info/uploads/posts/2017-10/1507743802_ukrainka.jpg", "123456"));
        listUser.add(new User(2, "Alina", "https://lime.apostrophe.ua/uploads/12102017/61ff42f39a91043bfda5b1e068705760.jpg", "123456"));
        listUser.add(new User(3, "Marina", "https://pink.ua/images/2015/11/pSRV_s2KRQ.jpg", "123456"));
        listUser.add(new User(4, "Natasha", "http://vz.ua/static/image/big/1375201056.jpg", "123456"));

    }

    public void save(User user) {
        user.setId(listUser.size() + 1);
        listUser.add(user);
        System.out.println("All users: " + listUser);
    }

    public List<User> getAll() {
        return new ArrayList<>(listUser);
    }

    public User getById(int id) {
        for (User user : listUser) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }
}
//