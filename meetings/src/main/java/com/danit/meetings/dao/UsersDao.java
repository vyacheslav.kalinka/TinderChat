package com.danit.meetings.dao;

import com.danit.meetings.entity.User;

import java.util.List;

public interface UsersDao {

  void save(User user);


  List<User> getAll();
}
//