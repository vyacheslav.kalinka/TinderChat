package com.danit.meetings.servlet;

import com.danit.meetings.dao.UserDao;
import com.danit.meetings.entity.User;
import com.danit.meetings.templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;


public class UserServlet extends HttpServlet {

  UserDao userDao = new UserDao();
  List<User> listUser = userDao.getAll();

  static List<User> likedUsers = new ArrayList<>();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Random random = new Random();
    int index = random.nextInt(listUser.size());
    User user = listUser.get(index);

    PageGenerator pageGenerator = PageGenerator.instance();
    Map<String, Object> map = new HashMap<>();
    map.put("user", user);
    String page = pageGenerator.getPage("index.html", map);
    resp.getWriter().write(page);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    int id = Integer.parseInt(req.getParameter("liked"));
    User user = getUser(id);
    likedUsers.add(user);
    resp.sendRedirect("/user");
  }

  private User getUser(int id) {
    for (User user : listUser) {
      if (id == user.getId()) {
        return user;
      }
    }
    return null;
  }
}
//