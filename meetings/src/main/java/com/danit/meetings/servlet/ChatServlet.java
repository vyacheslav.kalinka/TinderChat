package com.danit.meetings.servlet;

import com.danit.meetings.dao.UserDao;
import com.danit.meetings.entity.User;
import com.danit.meetings.templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ChatServlet extends HttpServlet{
    private UserDao userDao = new UserDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String requestURI = req.getRequestURI();
    String userIdStr = requestURI.substring(requestURI.lastIndexOf("/") +1);
    int userId = Integer.parseInt(userIdStr);
        User userToChat = userDao.getById(userId);

        PageGenerator pageGenerator = PageGenerator.instance();
        Map<String, Object> map = new HashMap<>();
        map.put("user", userToChat);
        String page = pageGenerator.getPage("chat.html", map);
        resp.getWriter().write(page);

        System.out.println(requestURI);
    }
}
//