package com.danit.meetings.servlet;

import com.danit.meetings.dao.UserDao;
import com.danit.meetings.entity.User;
import com.danit.meetings.templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class LikeServlet extends HttpServlet {


  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    List<User> likedUsers = UserServlet.likedUsers;

    PageGenerator pageGenerator = PageGenerator.instance();
    Map<String, Object> map = new HashMap<>();
    map.put("users", likedUsers);
    String page = pageGenerator.getPage("index2.html", map);
    resp.getWriter().write(page);

  }

}
//