package com.danit.meetings.entity;

public class User {

  private int id;
  private String name;
  private String src;
  private String pass;

  public User(int id, String name, String src, String pass) {
    this.id = id;
    this.name = name;
    this.src = src;
    this.pass = pass;
  }

  public User(){

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSrc() {
    return src;
  }

  public void setSrc(String src) {
    this.src = src;
  }

  public String getPass() {
    return pass;
  }

  public void setPass(String pass) {
    this.pass = pass;
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", src='" + src + '\'' +
        ", pass='" + pass + '\'' +
        '}';
  }
}
//