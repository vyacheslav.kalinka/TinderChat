package com.danit.meetings;

import com.danit.meetings.servlet.LikeServlet;
import com.danit.meetings.servlet.ChatServlet;
import com.danit.meetings.servlet.LikeServlet;
import com.danit.meetings.servlet.UserServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class Starter {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);

        ServletContextHandler handler = new ServletContextHandler();

        ServletHolder userHolder = new ServletHolder(new UserServlet());
        handler.addServlet(userHolder, "/user");

    ServletHolder likedHolder = new ServletHolder(new LikeServlet());
    handler.addServlet(likedHolder, "/liked");

        ServletHolder chatHolder = new ServletHolder(new ChatServlet());
        handler.addServlet(chatHolder, "/chat/*");

        server.setHandler(handler);
        server.start();

        server.join();
    }
}
                      //